package com.patterns.builder.fluent.entities;

public class Car {
    private String name;
    private String model;
    private String color;
    private String category;
    private int yearOfProduction;
    private int mileage;
    private int horsePower;
    private String fuelType;

    private Car(Builder builder) {
        this.name = builder.name;
        this.model = builder.model;
        this.color = builder.color;
        this.category = builder.category;
        this.yearOfProduction = builder.yearOfProduction;
        this.mileage = builder.mileage;
        this.horsePower = builder.horsePower;
        this.fuelType = builder.fuelType;
    }

    public static class Builder {
        private final String name;
        private final String model;
        private final String color;
        private String category;
        private int yearOfProduction;
        private int mileage;
        private int horsePower;
        private String fuelType;

        public Builder(String name, String model, String color) {
            this.name = name;
            this.model = model;
            this.color = color;
        }

        public Builder category(String category) {
            this.category = category;
            return this;
        }

        public Builder yearOfProduction(int yearOfProduction) {
            this.yearOfProduction = yearOfProduction;
            return this;
        }

        public Builder mileage(int mileage) {
            this.mileage = mileage;
            return this;
        }

        public Builder horsePower(int horsePower) {
            this.horsePower = horsePower;
            return this;
        }

        public Builder fuelType(String fuelType) {
            this.fuelType = fuelType;
            return this;
        }

        public Car build() {
            return new Car(this);
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", category='" + category + '\'' +
                ", yearOfProduction=" + yearOfProduction +
                ", mileage=" + mileage +
                ", horsePower=" + horsePower +
                ", fuelType='" + fuelType + '\'' +
                '}';
    }
}
