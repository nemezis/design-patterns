package com.patterns.builder.fluent.client;

import com.patterns.builder.fluent.entities.Car;

public class Client {
    public static void main(String[] args){
        Car fiatPanda = new Car.Builder("Fiat","Panda","Black&White")
                .category("Familly Car")
                .build();

        System.out.println(fiatPanda);


        Car ferrari = new Car.Builder("Ferrari", "Enzo", "Red")
                .category("Race Car")
                .yearOfProduction(2001)
                .fuelType("gazolina")
                .horsePower(500)
                .mileage(1)
                .build();

        System.out.println(ferrari);
    }
}
