package com.patterns.builder.problem.client;

import com.patterns.builder.problem.entities.Car;

public class Client {
    public static void main(String[] args) {
        // Example 1:
        //Tworzymy obiekt, nastepnie ustawiamy poszczegolne pola
        Car multipla = new Car();
        multipla.setName("Fiat");
        multipla.setModel("Multipla");
        multipla.setColor("Yellow");
        // Pokaz samochod
        System.out.println("Example 1: " + multipla);

        // Example 2: NOWE WYMAGANIE "Samochod musi miec nazwe i model!"
        //Tworzymy obiekt z ustawionymi polami
        Car raceCar = new Car("Ferrari", "Enzo");
        // Pokaz samochod
        System.out.println("Example 2a: " + raceCar);
        // Ustawiamy color samochodu
        raceCar.setColor("RED");
        // Pokaz samochod
        System.out.println("Example 2b: " + raceCar);

        // Example 3: NOWE WYMAGANIE "Samochod musi miec nazwe i model oraz kolor!"
        //Tworzymy obiekt z ustawionymi polami
        Car fiat126p = new Car("Fiat", "126p", "Green");
        // Pokaz samochod
        System.out.println("Example 3: " + fiat126p);

    }
}