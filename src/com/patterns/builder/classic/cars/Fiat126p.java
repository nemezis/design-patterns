package com.patterns.builder.classic.cars;

import com.patterns.builder.classic.entities.Car;
import com.patterns.builder.classic.entities.Engine;
import com.patterns.builder.classic.entities.Tires;

public class Fiat126p implements CarBuilder {

    private Car car;

    public Fiat126p() {
        this.car = new Car();
    }

    @Override
    public void buildTires() {
        Tires tire = new Tires();
        tire.setType("Normal");
        tire.setDurability(100);

        car.setTires(tire);
    }

    @Override
    public void buildEngine() {
        Engine engine = new Engine();
        engine.setType("v8");

        car.setEngine(engine);
    }

    @Override
    public Car getCar() {
        return car;
    }
}
