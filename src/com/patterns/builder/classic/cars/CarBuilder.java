package com.patterns.builder.classic.cars;

import com.patterns.builder.classic.entities.Car;

public interface CarBuilder {
    public void buildTires();

    public void buildEngine();

    public Car getCar();
}
