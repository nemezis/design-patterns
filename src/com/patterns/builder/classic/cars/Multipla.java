package com.patterns.builder.classic.cars;

import com.patterns.builder.classic.entities.Car;
import com.patterns.builder.classic.entities.Engine;
import com.patterns.builder.classic.entities.Tires;

/**
 * Created by piotr.sikorski on 2016-09-08.
 */
public class Multipla implements CarBuilder {
    private Car car;

    public Multipla() {
        this.car = new Car();
    }

    @Override
    public void buildTires() {
        Tires tires = new Tires();
        tires.setType("Slicks");
        tires.setDurability(50);

        car.setTires(tires);
    }

    @Override
    public void buildEngine() {
        Engine engine = new Engine();
        engine.setType("v12");

        car.setEngine(engine);
    }

    @Override
    public Car getCar() {
        return car;
    }
}
