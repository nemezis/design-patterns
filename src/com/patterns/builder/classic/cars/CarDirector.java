package com.patterns.builder.classic.cars;

import com.patterns.builder.classic.entities.Car;

public class CarDirector {
    private CarBuilder carBuilder;

    public CarDirector(CarBuilder carBuilder) {
        this.carBuilder = carBuilder;
    }

    public void makeCar() {
        carBuilder.buildTires();
        carBuilder.buildEngine();
    }

    public Car getCar() {
        return this.carBuilder.getCar();
    }
}
