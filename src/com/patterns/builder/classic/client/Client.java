package com.patterns.builder.classic.client;

import com.patterns.builder.classic.cars.CarBuilder;
import com.patterns.builder.classic.cars.CarDirector;
import com.patterns.builder.classic.cars.Fiat126p;
import com.patterns.builder.classic.cars.Multipla;

public class Client {
    public static void main(String[] args) {
        // CarBuilder carBuilder = new Fiat126p();
        CarBuilder carBuilder = new Multipla();
        CarDirector carDirector = new CarDirector(carBuilder);
        carDirector.makeCar();

        System.out.println(carDirector.getCar());
    }
}
